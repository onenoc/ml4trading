import hashlib
import os
import pickle as pkl
import time
import datetime as dt

#NOTE: need to make sure that the os library calls work on linux and windows as well as mac

class memoizer(object):
	def __init__(self, f):
		self.f = f
	
	def __call__(self, *args):
		#get cache filename based on function name and arguments
		scratchdir = os.environ
		h = hashlib.md5(self.f.__name__ + str(args))
		cachefilename = "Data/" + h.hexdigest() + '.pkl'
		#get based on same, but with "NO"
		h_no = hashlib.md5(self.f.__name__ + str(args) + 'NO')
		nocachefilename = "Data/" + h_no.hexdigest() + '.pkl'
		reafile = False
		if os.path.isfile(cachefilename):
			#if we find the cached file, read from it and return the data
			print "file already exists"
			cachefile = open(cachefilename, "rb")
			retval = pkl.load(cachefile)
			readfile = True
			cachefile.close()
			return retval
		else:
			#if the file exists telling us not to cache, we calculate
			if os.path.isfile(nocachefilename):
				print "have the no cache filename"
				#open and close this file so that it is marked as opened
				nocachefile = open(nocachefilename, "rb")
				nocachefile.close()
				return self.f(*args)
			self.__manage_directory_size()

			cachefile = open(cachefilename, "wb")
			#calculate return value and log time
			start_calc = time.time()
			retval = self.f(*args)
			calc_time = time.time() - start_calc
			#cache and log time
			start_cache = time.time()
			pkl.dump(retval, cachefile, -1)
			os.chmod(cachefilename, 0666)
			cache_time = time.time() - start_cache
			cachefile.close()
			#if the cache time is slower than the calculate time, create a file telling us not to use cache in future and delete cache file
			if cache_time > calc_time:
				nocachefile = open(nocachefilename, "wb")
				nocachefile.close()
				os.remove(cachefilename)
			#make sure that we don't have issue that we are writing a file that is bigger than disk space
			return retval
	def __manage_directory_size(self):
			#get the size of the data directory and filenames
			dirs = os.listdir("Data/")
			dir_size = []
			files = []
			for file in dirs:
				dir_size.append(os.path.getsize("Data/" + file))
				files.append(file)
			total_dir_size = sum(dir_size)
			#print total_dir_size
			#check whether it takes up over 5% of available disk space (this will need to be cross-platform)
			#http://stackoverflow.com/questions/4274899/get-actual-disk-space
			#http://code.activestate.com/recAipes/577972-disk-usage/
			st = os.statvfs("/")
			free_space = st.f_bavail * st.f_frsize
			#directory_portion =  total_dir_size / float(free_space)
			#if so, delete from last accessed file onwards
			#currently, we will set it so that if the size of the folder is more than 6152 bytes, reduce to 6152 bytes
			if total_dir_size > 6152:
				print "total dir size too big"
				print total_dir_size
				# st_atime time of most recent access
				#sort by last accessed: make sure actually last accessed
				files.sort(key = lambda x: os.stat("Data/" + x).st_atime)
				files = list(reversed(files))
				print type(files) 
				i = 0
				files_to_delete = []
				while total_dir_size > 6152 and i < len(files):
					total_dir_size -= os.path.getsize("Data/" + files[i])
					files_to_delete.append(files[i])
				for file in files_to_delete:
					os.remove("Data/" + file)
					print "file removed"
				print "new total dir size"
				print total_dir_size					


@memoizer
def slow(x):
	a = 0
	for i in range(x):
		for j in range(x):
			for k in range(x):
				a += 1
	return [a]

for i in range(50, 55):
	slow(i)
